FORMAT: 1A
HOST: http://localhost:3000/

# 旅ログAPI by RoR

旅ログに関するAPIドキュメント

#### 現状できること

+ ソーシャルログイン(登録・変更・削除・ログイン・ログアウト)
+ ユーザー情報の表示
+ 施設情報の登録・変更・表示・削除
+ 施設情報への評価(いいね)
+ 施設情報の検索

# Group Users

## users [/users]

### PUT: ユーザー情報の変更 [PUT]

+ Request (application/json)

        {
            "data":{
                "user":{
                    "uid":"プロバイダー由来のid",
                    "provider":"facebook　or twitter",
                    "name":"ユーザー名",
                    "image":"画像URL"
                }
            }
        }

+ Response 201 (application/json)

        {
            "status":201
        }

### DELETE: ユーザーの削除 [DELETE]

+ Response 203 (application/json)

        {
            "status":203
        }


## users/auth/{provider} [/users/auth/{provider}]
providerはtwitterもしくはfacebook
### GET: ログイン [GET]

+ Response 200 (application/json)

        {
            "data":{
                "user":{
                    "uid":"プロバイダー由来のid",
                    "provider":"facebook　or twitter",
                    "name":"ユーザー名",
                    "image":"画像URL"
                }
            },
            "status":200
        }

## users/sign_out [/users/sign_out]

### DELETE: ログアウト [DELETE]

+ Response 203 (application/json)

        {
            "status":203
        }

## users/show/{id} [/users/show/{id}]

### GET: ユーザー情報の表示 [GET]

+ Response 200 (application/json)

        {
            "data":{
                "user":{
                    "uid":"プロバイダー由来のid",
                    "provider":"facebook　or twitter",
                    "name":"ユーザー名",
                    "image":"画像URL"
                }
            },
            "status":200
        }

+ Response 404 (application/json)

        {
            "message":"Not Found",
            "status":404
        }
        
## users/password [/users/password]

### PUT: パスワードの変更 [PUT]

+ Response 201 (application/json)

        {
            "status":201
        }

# Group Leisures

## leisures [/leisures]

### GET: レジャー施設の一覧 [GET]

+ Response 201 (application/json)

        {
            "date" {
                "leisure" [
                    {
                        "id":1,
                        "name":"観光地 1",
                        "address":"観光地 1",
                        "description":"説明文 1",
                        "tell":"0120-000-000",
                        "hours":"営業時間"
                        "category":"カテゴリー1"
                    },
                    {
                        "id":2,
                        "name":"観光地 2",
                        "address":"観光地 2",
                        "description":"説明文 2",
                        "tell":"0120-111-111",
                        "hours":"営業時間"
                        "category":"カテゴリー２"
                    }
                ] 
            },
            "status":200
        }

### POST: レジャー施設の新規作成 [POST]

+ Request (application/json)

        {

            "leisure":{
                "id":1,
                "name":"観光地 1",
                "address":"観光地 1",
                "description":"説明文 1",
                "tell":"0120-000-000",
                "hours":"営業時間"
                "category":"カテゴリー1"
            }
        }

+ Response 201 (application/json)

        {
            "status":201
        }

## leisures/{id} [/leisures/{id}]

### GET: 各レジャー施設情報の表示 [GET]

+ Response 200 (application/json)

        {
            "data":{
                "leisure":{
                    "id":1,
                    "name":"観光地 1",
                    "address":"観光地 1",
                    "description":"説明文 1",
                    "tell":"0120-000-000",
                    "hours":"営業時間"
                    "category":"カテゴリー1"
                }
            },
            "status":200
        }

+ Response 404 (application/json)

        {
            "message":"Not Found",
            "status":404
        }

### PUT: 各レジャー施設情報の変更 [PUT]

+ Request (application/json)

        {
            "leisure":{
                "id":1,
                "name":"観光地 1",
                "address":"観光地 1",
                "description":"説明文 1",
                "tell":"0120-000-000",
                "hours":"営業時間"
                "category":"カテゴリー1"
            }
        }

+ Response 201 (application/json)

        {
            "status":201
        }

### DELETE: 各レジャー施設情報の削除 [DELETE]

+ Response 203 (application/json)

        {
            "status":203
        }

## leisures/{id}/like [/leisures/{id}/like]

### GET: 各レジャー施設にいいね！ [GET]

+ Response 203 (application/json)

        {
            "status":203
        }

## leisures/search?{query} [/leisures/search]

### GET: レジャー施設情報の検索 [GET]

検索に使用できるクエリは
<table>
    <tr><th>名前</th><td>name=AAAAA</td></tr>
    <tr><th>カテゴリー</th><td>cat=BBBBB</td></tr>
</table>

+ Response 200

        {
            "date" {
                "leisure" [
                    {
                        "id":1,
                        "name":"観光地 1",
                        "address":"観光地 1",
                        "description":"説明文 1",
                        "tell":"0120-000-000",
                        "hours":"営業時間"
                        "category":"カテゴリー1"
                    },
                    {
                        "id":2,
                        "name":"観光地 2",
                        "address":"観光地 2",
                        "description":"説明文 2",
                        "tell":"0120-111-111",
                        "hours":"営業時間"
                        "category":"カテゴリー２"
                    }
                ] 
            },
            "status":200
        }

# Group Users

## users [/users]

### PUT: ユーザー情報の変更 [PUT]

+ Request (application/json)

        {
            "data":{
                "user":{
                    "uid":"プロバイダー由来のid",
                    "provider":"facebook　or twitter",
                    "name":"ユーザー名",
                    "image":"画像URL"
                }
            }
        }

+ Response 201 (application/json)

        {
            "status":201
        }

### DELETE: ユーザーの削除 [DELETE]

+ Response 203 (application/json)

        {
            "status":203
        }


## users/auth/{provider} [/users/auth/{provider}]
providerはtwitterもしくはfacebook
### GET: ログイン [GET]

+ Response 200 (application/json)

        {
            "data":{
                "user":{
                    "uid":"プロバイダー由来のid",
                    "provider":"facebook　or twitter",
                    "name":"ユーザー名",
                    "image":"画像URL"
                }
            },
            "status":200
        }

## users/sign_out [/users/sign_out]

### DELETE: ログアウト [DELETE]

+ Response 203 (application/json)

        {
            "status":203
        }

## users/show/{id} [/users/show/{id}]

### GET: ユーザー情報の表示 [GET]

+ Response 200 (application/json)

        {
            "data":{
                "user":{
                    "uid":"プロバイダー由来のid",
                    "provider":"facebook　or twitter",
                    "name":"ユーザー名",
                    "image":"画像URL"
                }
            },
            "status":200
        }

+ Response 404 (application/json)

        {
            "message":"Not Found",
            "status":404
        }
        
## users/password [/users/password]

### PUT: パスワードの変更 [PUT]

+ Response 201 (application/json)

        {
            "status":201
        }

# Group Leisures

## leisures [/leisures]

### GET: レジャー施設の一覧 [GET]

+ Response 201 (application/json)

        {
            "date" {
                "leisure" [
                    {
                        "id":1,
                        "name":"観光地 1",
                        "address":"観光地 1",
                        "description":"説明文 1",
                        "tell":"0120-000-000",
                        "hours":"営業時間"
                        "category":"カテゴリー1"
                    },
                    {
                        "id":2,
                        "name":"観光地 2",
                        "address":"観光地 2",
                        "description":"説明文 2",
                        "tell":"0120-111-111",
                        "hours":"営業時間"
                        "category":"カテゴリー２"
                    }
                ] 
            },
            "status":200
        }

### POST: レジャー施設の新規作成 [POST]

+ Request (application/json)

        {

            "leisure":{
                "id":1,
                "name":"観光地 1",
                "address":"観光地 1",
                "description":"説明文 1",
                "tell":"0120-000-000",
                "hours":"営業時間"
                "category":"カテゴリー1"
            }
        }

+ Response 201 (application/json)

        {
            "status":201
        }

## leisures/{id} [/leisures/{id}]

### GET: 各レジャー施設情報の表示 [GET]

+ Response 200 (application/json)

        {
            "data":{
                "leisure":{
                    "id":1,
                    "name":"観光地 1",
                    "address":"観光地 1",
                    "description":"説明文 1",
                    "tell":"0120-000-000",
                    "hours":"営業時間"
                    "category":"カテゴリー1"
                }
            },
            "status":200
        }

+ Response 404 (application/json)

        {
            "message":"Not Found",
            "status":404
        }

### PUT: 各レジャー施設情報の変更 [PUT]

+ Request (application/json)

        {
            "leisure":{
                "id":1,
                "name":"観光地 1",
                "address":"観光地 1",
                "description":"説明文 1",
                "tell":"0120-000-000",
                "hours":"営業時間"
                "category":"カテゴリー1"
            }
        }

+ Response 201 (application/json)

        {
            "status":201
        }

### DELETE: 各レジャー施設情報の削除 [DELETE]

+ Response 203 (application/json)

        {
            "status":203
        }

## leisures/{id}/like [/leisures/{id}/like]

### GET: 各レジャー施設にいいね！ [GET]

+ Response 203 (application/json)

        {
            "status":203
        }

## leisures/search?{query} [/leisures/search]

### GET: レジャー施設情報の検索 [GET]

検索に使用できるクエリは
<table>
    <tr><th>名前</th><td>name=AAAAA</td></tr>
    <tr><th>カテゴリー</th><td>cat=BBBBB</td></tr>
</table>

+ Response 200

        {
            "date" {
                "leisure" [
                    {
                        "id":1,
                        "name":"観光地 1",
                        "address":"観光地 1",
                        "description":"説明文 1",
                        "tell":"0120-000-000",
                        "hours":"営業時間"
                        "category":"カテゴリー1"
                    },
                    {
                        "id":2,
                        "name":"観光地 2",
                        "address":"観光地 2",
                        "description":"説明文 2",
                        "tell":"0120-111-111",
                        "hours":"営業時間"
                        "category":"カテゴリー２"
                    }
                ] 
            },
            "status":200
        }


<!-- 
curl -H "Content-Type: application/json" 
-d '{"location":{"id":"id","name":"ユーザー名","address":"住所","description":"説明"}}' http://localhost:3000/locations

curl -X DELETE http://localhost:3000/locations/6 
-->
                  Prefix Verb     URI Pattern                            Controller#Action
              home_index GET      /home/index(.:format)                  home#index
         leisures_search GET      /leisures/search(.:format)             leisures#search
                leisures GET      /leisures(.:format)                    leisures#index
                         POST     /leisures(.:format)                    leisures#create
             new_leisure GET      /leisures/new(.:format)                leisures#new
            edit_leisure GET      /leisures/:id/edit(.:format)           leisures#edit
                 leisure GET      /leisures/:id(.:format)                leisures#show
                         PATCH    /leisures/:id(.:format)                leisures#update
                         PUT      /leisures/:id(.:format)                leisures#update
                         DELETE   /leisures/:id(.:format)                leisures#destroy
               user_show GET      /users/show/:id(.:format)              users#show
        new_user_session GET      /users/sign_in(.:format)               users/sessions#new
            user_session POST     /users/sign_in(.:format)               users/sessions#create
    destroy_user_session DELETE   /users/sign_out(.:format)              users/sessions#destroy
 user_omniauth_authorize GET|POST /users/auth/:provider(.:format)        users/omniauth_callbacks#passthru {:provider=>/facebook|twitter/}
  user_omniauth_callback GET|POST /users/auth/:action/callback(.:format) users/omniauth_callbacks#(?-mix:facebook|twitter)
           user_password POST     /users/password(.:format)              users/passwords#create
       new_user_password GET      /users/password/new(.:format)          users/passwords#new
      edit_user_password GET      /users/password/edit(.:format)         users/passwords#edit
                         PATCH    /users/password(.:format)              users/passwords#update
                         PUT      /users/password(.:format)              users/passwords#update
cancel_user_registration GET      /users/cancel(.:format)                users/registrations#cancel
       user_registration POST     /users(.:format)                       users/registrations#create
   new_user_registration GET      /users/sign_up(.:format)               users/registrations#new
  edit_user_registration GET      /users/edit(.:format)                  users/registrations#edit
                         PATCH    /users(.:format)                       users/registrations#update
                         PUT      /users(.:format)                       users/registrations#update
                         DELETE   /users(.:format)                       users/registrations#destroy
