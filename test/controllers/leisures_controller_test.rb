require 'test_helper'

class LeisuresControllerTest < ActionController::TestCase
  setup do
    @leisure = leisures(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:leisures)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create leisure" do
    assert_difference('Leisure.count') do
      post :create, leisure: { address: @leisure.address, name: @leisure.name }
    end

    assert_redirected_to leisure_path(assigns(:leisure))
  end

  test "should show leisure" do
    get :show, id: @leisure
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @leisure
    assert_response :success
  end

  test "should update leisure" do
    patch :update, id: @leisure, leisure: { address: @leisure.address, name: @leisure.name }
    assert_redirected_to leisure_path(assigns(:leisure))
  end

  test "should destroy leisure" do
    assert_difference('Leisure.count', -1) do
      delete :destroy, id: @leisure
    end

    assert_redirected_to leisures_path
  end
end
