# coding: utf-8

# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
5.times do |no|
#   Category.create(:name => "タイトル #{no}")
   Leisure.create(:name => "観光地 #{no+1}",:description => "説明文 #{no+1}", :address => "住所 #{no+1}", :hours => "営業時間　#{no+1}", :tell => "電話番号 #{no+1}", :category => "カテゴリー #{no%3}")
end
