class CreateLeisures < ActiveRecord::Migration
  def change
    create_table :leisures do |t|
      t.string :name
      t.string :address
      t.string :hours
      t.string :tell
      t.text :description
      t.string :category

      t.timestamps null: false
    end
  end
end
