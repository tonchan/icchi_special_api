class Leisure < ActiveRecord::Base
	acts_as_taggable
	has_reputation :likes, source: :user, aggregated_by: :sum
	validates :name, presence: true, uniqueness: true
	validates :tell, :format=>{:with=>/\A[0-9-]/, :allow_blank=>true}

	def self.search(query_name="",query_cat="")
		if query_cat && query_name
			self.where(category: query_cat).where(['name LIKE ?', "%#{query_name}%"])
		elsif query_cat
			self.where(category: query_cat)
		elsif query_name
			self.where(['name LIKE ?', "%#{query_name}%"])
		else
			self.all
		end
	end
end
