class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
  :recoverable, :rememberable, :trackable, :validatable,:omniauthable
  has_many :evaluations, class_name: "ReputationSystem::Evaluation", as: :source

  def self.find_for_oauth(auth)
  	user = User.where(uid: auth.uid, provider: auth.provider).first
  	unless user
  		user = User.create(
  			uid: auth.uid,
  			provider: auth.provider,
  			name: auth.info.name,
        image: auth.info.image,
  			email: User.get_email(auth),
  			password: Devise.friendly_token[4, 30])
  	end
  	user
  end

  def self.liked?(post)
    evaluations.where(target_type: post.class, reputation_name: :likes, target_id: post.id).present?
  end

  private
  def self.get_email(auth)
  	email = auth.info.email
  	email = "#{auth.provider}-#{auth.uid}@example.com" if email.blank?
  	email
  end
end
