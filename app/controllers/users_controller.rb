class UsersController < ApplicationController
	before_action :authenticate_user!, only: [:show]

  # GET /users/1
  def show
  	user = find_user(params[:uid])
    if user
    	responce_json = {
    		data: {
    			user: {
    				name: user.name,
    				uid: user.uid,
    				provider: user.provider,
    				image: user.image
    			}
    		},
    		status: 200
    	}
    	render :json => responce_json
    end
  end

  private
  def find_user(uid)
    user = User.where(uid: uid).first
    render_error(404,"Not Found") unless user
    return user
  end
end
