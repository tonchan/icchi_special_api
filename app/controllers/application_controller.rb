class ApplicationController < ActionController::Base
	# Prevent CSRF attacks by raising an exception.
	# For APIs, you may want to use :null_session instead.
	protect_from_forgery with: :null_session

	private
	def render_error(status, msg)
		render :json => {:message => msg, :status => status}
	end

	def authenticate_#{mapping}!(opts={})
		render_error(401,"Unauthorized") unless user_signed_in?
	end
end
