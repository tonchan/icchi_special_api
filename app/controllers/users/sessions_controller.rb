class Users::SessionsController < Devise::SessionsController
	def new
		super
	end
	
  def create
    self.resource = warden.authenticate!(auth_options)
    #set_flash_message(:notice, :signed_in) if is_flashing_format?
    sign_in(resource_name, resource)
    #yield resource if block_given?
    #respond_with resource, location: after_sign_in_path_for(resource)
    render :json => { message: :no_content, status: 204}
  end
	
  def destroy
    signed_out = (Devise.sign_out_all_scopes ? sign_out : sign_out(resource_name))
    #set_flash_message :notice, :signed_out if signed_out && is_flashing_format?
    #yield if block_given?
    #respond_to_on_destroy
    render :json => { message: :no_content, status: 204}
  end
end