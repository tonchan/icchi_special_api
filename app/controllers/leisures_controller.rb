class LeisuresController < ApplicationController
  before_action :set_leisure, only: [:show, :edit, :update, :destroy, :like]

  # GET /leisures
  def index
    @leisures = Leisure.all
    render_leisures
  end

  def search
    @leisures = Leisure.search(params[:name],params[:cat])
    render_leisures
  end

  # GET /leisures/1
  def show
    render_leisure 200
  end

  #??? GET /leisures/new
  def new
    @leisure = Leisure.new
  end

  #??? GET /leisures/1/edit
  def edit
  end

  # POST /leisures
  def create
    @leisure = Leisure.new(leisure_params)

    if @leisure.save
      render_leisure 201
    else
      render_error(400,@leisure.errors)
    end
  end

  # PATCH/PUT /leisures/1
  def update
    if @leisure.update(leisure_params)
      render_leisure 201
    else
      render_error(400,@leisure.errors)
    end
  end

  def like
    @leisure.add_evaluation(:likes, 1, current_user) unless @leisure.delete_evaluation(:likes, current_user)
    render_leisure 201
    #@leisure.reputation_for(:likes).to_i
  end

  # DELETE /leisures/1
  def destroy
    @leisure.destroy
    render_leisure 201
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_leisure
      @leisure = Leisure.find_by_id(params[:id])
      render_error(404,"Not Found") unless @leisure
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def leisure_params
      params.require(:leisure).permit(:name, :address, :description)
    end

    def render_leisures
      responce_json = {data:{leisure:[]},status: 200}
      @leisures.each do |l|
        responce_json[:data][:leisure].push({
          id: l.id,
          name: l.name,
          description: l.description,
          address: l.address,
          tell: l.tell,
          hours: l.hours,
          category: l.category,
          tag: l.tag_list,
          like: l.reputation_for(:likes).to_i
          })
      end
      render :json => responce_json
    end

    def render_leisure(status_num)
      responce_json = {
        data: {
          leisure: {
            name: @leisure.name,
            id: @leisure.id,
            description: @leisure.description,
            tell: @leisure.tell,
            hours: @leisure.hours,
            category: @leisure.category,
            tag: @leisure.tag_list,
            like: @leisure.reputation_for(:likes).to_i
          }
          },
          status: status_num
        }
        render :json => responce_json
      end
    end