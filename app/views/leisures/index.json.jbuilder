json.array!(@leisures) do |leisure|
  json.extract! leisure, :id, :name, :address
  json.url leisure_url(leisure, format: :json)
end
